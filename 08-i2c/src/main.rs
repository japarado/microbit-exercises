#![deny(unsafe_code)]
#![no_std]
#![no_main]

extern crate panic_rtt_target;

use cortex_m_rt::entry;
use rtt_target::rtt_init_print;

#[entry]
fn main() -> !
{
    rtt_init_print!();

    loop
    {}
}

#![deny(unsafe_code)]
#![no_main]
#![no_std]

extern crate panic_rtt_target;

use core::fmt::Write;

use cortex_m_rt::entry;
use microbit::display::blocking::Display;
use microbit::hal::prelude::InputPin;
use microbit::hal::uarte::{Baudrate, Parity};
use microbit::hal::{Timer, Uarte};
use microbit::Board;
use rtt_target::rtt_init_print;

use serial_common::UartePort;

#[entry]
fn main() -> !
{
    rtt_init_print!();

    let board = Board::take().unwrap();
    let mut timer = Timer::new(board.TIMER0);
    let mut display = Display::new(board.display_pins);

    let button_a = board.buttons.button_a;
    let button_b = board.buttons.button_b;

    let mut serial = {
        let serial = Uarte::new(
            board.UARTE0,
            board.uart.into(),
            Parity::EXCLUDED,
            Baudrate::BAUD115200,
        );
        UartePort::new(serial)
    };

    let leds = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ];

    let leds_a = [
        [1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0],
    ];

    let leds_b = [
        [0, 0, 0, 0, 1],
        [0, 0, 0, 0, 1],
        [0, 0, 0, 0, 1],
        [0, 0, 0, 0, 1],
        [0, 0, 0, 0, 1],
    ];

    let leds_both = [
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
    ];

    let duration = 10;

    loop
    {
        if button_a.is_low().unwrap() && !button_b.is_low().unwrap()
        {
            display.show(&mut timer, leds_a, duration);
            write!(serial, "A\r\n").unwrap();
        }
        else if button_b.is_low().unwrap() && !button_a.is_low().unwrap()
        {
            display.show(&mut timer, leds_b, duration);
            write!(serial, "B\r\n").unwrap();
        }
        else if button_a.is_low().unwrap() && button_b.is_low().unwrap()
        {
            display.show(&mut timer, leds_both, duration);
            write!(serial, "BOTH\r\n").unwrap();
        }
    }
}

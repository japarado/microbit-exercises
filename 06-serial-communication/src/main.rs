#![no_std]
#![no_main]

extern crate panic_rtt_target;

use core::fmt::Write;

use cortex_m_rt::entry;
use microbit::hal::prelude::_embedded_hal_blocking_delay_DelayMs;
use microbit::hal::uarte::{Baudrate, Parity};
use microbit::hal::{Delay, Uarte};
use microbit::Board;
use rtt_target::rtt_init_print;
use serial_common::UartePort;

#[entry]
fn main() -> !
{
    rtt_init_print!();

    let board = Board::take().unwrap();
    let mut delay = Delay::new(board.SYST);

    let mut serial = {
        let serial = Uarte::new(
            board.UARTE0,
            board.uart.into(),
            Parity::EXCLUDED,
            Baudrate::BAUD115200,
        );
        UartePort::new(serial)
    };

    let mut counter = 0;

    loop
    {
        counter += 1;
        write!(serial, "Amogler {}\r\n", counter).unwrap();
        delay.delay_ms(1000_u32);
    }
}
